---
features:
  secondary:
  - name: "Static Analysis analyzer updates"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/analyzers'
    reporter: connorgilbert
    stage: secure
    categories:
    - Code Quality
    - SAST
    - Secret Detection
    issue_url: 'https://gitlab.com/gitlab-org/security-products/release/-/issues/122'
    description: |
        GitLab Static Analysis includes [many security analyzers](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks) that the GitLab Static Analysis team actively manages, maintains, and updates. The following analyzer updates were published during the 15.1 release milestone. These updates bring additional coverage, bug fixes, and improvements.

        - Secret Detection analyzer updated for better offline support and easier debugging. See [CHANGELOG](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/master/CHANGELOG.md#v404) for details.
            - Improve logging
            - Use checked-out copy of the repository if `git fetch` fails
            - Fall back to scanning the latest commit if automatic diff detection fails
        - SpotBugs analyzer updated to SpotBugs version 4.7.0 and find-sec-bugs version 1.12.0. See [CHANGELOG](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs/-/blob/master/CHANGELOG.md#v310) for details.
            - Update gradle and grails to support Java 17
            - Set Java 17 as the system-wide default version
            - Use 'assemble' task for Gradle projects, instead of 'build', to support custom `GRADLE_CLI_OPTS` (see [issue #299872](https://gitlab.com/gitlab-org/gitlab/-/issues/299872))
            - Add additional detection rules

        If you [include the GitLab-managed SAST template](https://docs.gitlab.com/ee/user/application_security/sast/#configure-sast-manually) ([`SAST.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml)), you don't need to do anything to receive these updates. However, if you override or customize your own CI/CD template, you need to update your CI/CD configurations. To remain on a specific version of any analyzer, you can [pin to a minor version of an analyzer](https://docs.gitlab.com/ee/user/application_security/sast/#pinning-to-minor-image-version). Pinning to a previous version prevents you from receiving automatic analyzer updates and requires you to manually bump your analyzer version in your CI/CD template.

        For previous changes, see [last month's updates](https://about.gitlab.com/releases/2022/05/22/gitlab-15-0-released/#static-analysis-analyzer-updates).
