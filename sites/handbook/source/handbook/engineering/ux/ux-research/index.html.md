---
layout: handbook-page-toc
title: "UX research at GitLab"
description: "The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The goal of UX research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations, and goals. We use these insights to inform and strengthen product and design decisions.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EQ750KX_6nU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

UX Researchers are one of the many GitLab Team Members who conduct user research. Other roles, such as Product Managers and Product Designers, frequently conduct research with guidance from the UX Research team. Product Designers and Product Managers should complete the [Research Shadowing](/handbook/engineering/ux/ux-research-training/research-shadowing/) process during their onboarding to gain an understanding of how research is conducted at GitLab. 

Do you have questions about UX Research? The UX Research team is here for you! Reach out in the #ux_research Slack channel.
