---
layout: handbook-page-toc
title: "Open Source Program"
description: Learn about the GitLab for Open Source Program and other open source programs from GitLab's Community Relations team.
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

# <i class="far fa-newspaper" id="biz-tech-icons"></i> About
GitLab's open source program is part of the [Community Relations team](/handbook/marketing/community-relations/). It consists of three sub-programs:

* [**GitLab for Open Source Program**](/handbook/marketing/community-relations/opensource-program/#-gitlab-for-open-source-program), through which qualifying open source projects [receive benefits](/solutions/open-source/) like GitLab Ultimate and 50,000 CI minutes for free.
* [**GitLab Open Source Partners**](/handbook/marketing/community-relations/opensource-program/#-gitlab-open-source-partners), a [partnership program](/solutions/open-source/partners) designed for large or prominent open source projects and organizations.
* [**Consortium Memberships**](/handbook/marketing/community-relations/opensource-program/#-consortium-memberships-and-sponsorships), which allow us to [extend GitLab's leadership in key open source initiatives](https://gitlab.com/gitlab-com/marketing/community-relations/open-source-program/consortium-memberships), enhance GitLab's brand, and/or improve engineering alignment

# <i class="far fa-paper-plane" id="biz-tech-icons"></i> How to reach us
 * DRI: [@bbehr](https://gitlab.com/bbehr)
 * Slack channel: #community-programs
 * Email: opensource@gitlab.com

# <i class="fas fa-tasks" id="biz-tech-icons"></i> What we're working on
We use epics, issue boards, and labels to track our work. We also maintain [quarterly objectives and key results](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OSS%20OKR).

See the [Community Relations project management page](/handbook/marketing/community-relations/project-management/) for more detail.

# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab for Open Source Program

By empowering open source projects with our most advanced features, the [GitLab for Open Source Program](/solutions/open-source/) supports GitLab's mission to make the world a place where anyone can contribute. We help make GitLab the best place for open source projects to grow and thrive.

Send questions about the GitLab for Open Source Program to `opensource@gitlab.com`.

## FAQs

### What are the benefits of the GitLab for Open Source Program?

Members of the GitLab for Open Source Program receive the following benefits at no cost:

* [GitLab Ultimate](/pricing/) (self-managed or SaaS) with unlimited seats per license
* 50,000 CI/CD minutes (users can purchase 1,000 [additional CI minutes](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes) for a one-time, $8 fee)

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> While members of the GitLab for Open Source Program receive GitLab Ultimate at no cost, they do not receive support as part of their license.
{: .alert .alert-warning}

### Who qualifies for the GitLab for Open Source Program?

In order to be accepted into the GitLab for Open Source Program, applicants must:

* **Use OSI-approved licenses for their projects:** Every project in the applying namespace must be published under an [OSI-approved open source license](https://opensource.org/licenses/alphabetical).
*  **Not seek profit:** An organization can accept donations to sustain its work, but it can’t seek to make a profit by selling services, by charging for enhancements or add-ons, or by other means.
* **Be publicly visible:** Both the applicant's GitLab.com group or self-managed instance and source code must be publicly visible and publicly available.

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> Benefits of the GitLab for Open Source Program apply to a namespace. To qualify, every project in an applicant’s namespace must carry an OSI-approved open source license.
{: .alert .alert-warning}

#### Qualification exceptions

* **Private Project Exceptions:** In some cases, we allow program members to host a small number of private projects if those projects contain sensitive data. Members should send an email to `opensource@gitlab.com` in order to discuss this exemption. Program members must obtain written permission from the GitLab Open Source Program team in order to use their licenses outside of program requirements.
* **Federal Exception Policy:** Unfortunately, we are not able to accept all open source projects that are affiliated with the US Federal government. Projects that are affiliated must work with a Sales representative to see if they qualify.
* **Strategic Qualification Exceptions:** We may make strategic exceptions to our program requirements. A GitLab Sales team member must make this request on behalf of an open source project. To request an execption, file an issue in the [GitLab for Open Source Program project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-for-open-source). Please select the `program-qualification-exception-request` template. when filing the issue. Account Executives and their managers must approve the exception request. Technical Account Managers (TAMs) associated with the account should also be notified of the exception request.

### What are the terms of the GitLab for Open Source Program?

Upon acceptance to the GitLab for Open Source Program, all program members are subject to the [GitLab for Open Source Program Agreement](https://about.gitlab.com/handbook/legal/opensource-agreement/).

### How does someone apply for the GitLab for Open Source Program?

Applicants should submit the form on the [GitLab for Open Source Program page](https://about.gitlab.com/solutions/open-source/join/).

As part of the application process, applicants must provide screenshots to confirm their eligibility. They should submit screenshots of:

* The project's license overview
* The project's license contents
* The project's public visibility settings

For more specific instructions on obtaining and submitting required screenshots, [see GitLab Docs](https://docs.gitlab.com/ee/subscriptions/#gitlab-for-open-source).

### How are GitLab for Open Source Program applications processed?

Gitlab uses SheerID, a trusted partner, to verify that applicants meet the GitLab for Open Source Program requirements. In most cases, applicants receive a decision on their application within 15 business days.

The GitLab for Open Source team will then follow the [community programs application workflow](/handbook/marketing/community-relations/community-operations/community-program-applications/) to process applications.

When verified, applicants receive a verification email containing specific instructions for obtaining their license.

### Must members of the GitLab for Open Source Program renew their memberships?

Yes. Program members must renew their memberships annually. If they don’t renew, [their accounts will be downgraded](https://about.gitlab.com/pricing/licensing-faq/#what-happens-when-my-subscription-is-about-to-expire-or-has-expired).

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59);"></i> We recommend that applicants begin the renewal process at least one month in advance of their renewal dates to ensure sufficient processing time. Note that applications will not be processed during U.S. holidays and responses may be delayed during those periods.
{: .alert .alert-warning}

### How does someone renew their membership in the GitLab for Open Soure Program?

To request a renewal, program members should email a renewal request to `opensource@gitlab.com`. If the requeest is urgent, please include `[Urgent]` in the email's subject line.

A renewal request should use the following template:

```
Subject: Renewal Request | Date of Expiration (MM/YYYY)

To help us find your account:

* Name of your organization or project: 
* Email associated with this account: 

To help us make sure you still qualify:

* Link to your publicly visible GitLab instance: 
* Link to one of your OSI-compliant licenses: 
* Written acceptance of this statement (Include this sentence in your request): `I confirm that my organization does not seek to make a profit from this OSS project`

To help us plan for next year:

* Number of seats you are renewing: 
* Please verify the license type to be renewed: 
* Please attach a PDF version of last year's quote if available
* List any change of ownership to the account:
(If account ownership details change, please send the new account holder's name, email address, and contact's mailing address)
      
```

When a renewal request is processed and accepted, applicants will be asked to sign a $0 renewal quote. After that:

* **For Saas renewals:** No further action is necessary.
* **For Self-managed renewals:** You'll need to download your license from the GitLab Customer Portal and upload it to your instance.

### How else can GitLab assist open source projects?

Members of the GitLab for Open Source Program may also be interested in:

* **Migration services:** Open source communities requiring assistance with their migration to GitLab can receive help from the GitLab team. See the [professional services page](/services/) for more information.
* **Technology partners:** Many of GitLab's [technology partners](/partners/technology-partners/) offer services GitLab doesn't provide, such as hosting for Community Edition instances. We've partnered with others to help expand the options for our users. Browse our [GitLab Alliance Partners](/partners/technology-partners/) to learn more.

### Where can members of the GitLab for Open Source Program find support?

While GitLab for Open Source Program benefits do not include product [support](/support/), program members can receive help with GitLab in a number of ways. Use the GitLab Community Programs support workflow](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs) to determine which is most useful in your case.

In general, we recommend the following:

* Send questions or issues requiring immediate attention or sensitive information to the GitLab for Open Source Program team at `opensource@gitlab.com`.
* Review [GitLab Docs](https://docs.gitlab.com/) for answers to general product-related questions.
* Post questions in the [GitLab for Open Source](https://forum.gitlab.com/c/community/gitlab-for-open-source/) category of the [GitLab Forum](https://forum.gitlab.com), where community members and GitLab team members can review and discuss them.
* File bug reports and breaking behaviors [as issues](https://gitlab.com/gitlab-org/gitlab/-/issues) for product teams to review and address.

## OSS product SKUs
The following active SKUs are related to the GitLab for Open Source Program:

 * `[OSS Program] SaaS - Ultimate (formerly Gold) - 1 Year`
 * `[OSS Program] Self-Managed - Ultimate - 1 Year`
 
# <i class="far fa-newspaper" id="biz-tech-icons"></i> GitLab Open Source Partners
The GitLab Open Source Partners program exists to build relationships with prominent open source projects. These projects are typically members of the GitLab for Open Source Program. Most partners use GitLab Ultimate (either SaaS or self-managed); however, some prefer using the fully open source [Community Edition](https://about.gitlab.com/install/ce-or-ee/) because of their strong commitment to using only open source tools.

Open source partners receive specific benefits by joining the program (see below). GitLab benefits from these partnerships when open source partners provide valuable feedback and data on their use of GitLab, even contribute to GitLab's open core. All parties jointly benefit when they're able to collaborate on community outreach, co-marketing, joint announcements, and special initiatives.

## FAQs

### What are the benefits of being a member of the GitLab Open Source Partners program?

Program members receive:

* Recognition on the [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/) and [GitLab Open Source Partners](https://about.gitlab.com/solutions/open-source/partners/) pages
* Support for migration to GitLab
* Exclusive invitations to participate in GitLab events
* Visibility and promotion through GitLab marketing channels
* Direct line of communication to GitLab

## Onboarding OSS Partners

### Email templates
We have a number of [email templates](https://docs.google.com/document/d/1et0t3CLQdT8I2UbxBVrpFqMXmnd6cEtMZuE61V1DiXM/edit#) to help us interact with prospective and current OSS Partners. These email templates are publicly visible to allow our community to learn from our onboarding process.

### Onboarding Packet
When an org has joined the OSS Partners program, we send them the [OSS Partners Onboarding Packet](https://docs.google.com/presentation/d/1VvRza5K_fAYTXVfxGF494cQM7vIUxSv7vPMTtmo-PmY/edit#slide=id.gd40831fb97_0_0).

### Contact Information
We need to make sure that we have the right contact information for our Open Source Partners.

Suggested touchpoints for requesting contact information and updates:
 * Onboarding new OSS Partners (within first 30 days) -- make sure initial contacts are given.
 * Once a year (during Q4) request a refresh

Some OSS Partners will not have contacts for each function. If that's the case, the contact for that function should be left blank and the Primary contact should be contacted instead.

Multiple contacts can be added for each of these functions except the Primary contact.

#### Contact functions
 * **Primary:** Represents OSS Partner at meetings and is our main line of contact. Main relationship owner.
 * **Alternate:** Will be contacted if we are unable to reach the Primary contact and will be the backup representative.
 * **Marketing:** Contacted when event and marketing opportunities for OSS Partners arise.
 * **Technical:** Contacted for participation in surveys or focus studies that require technical expertise, or when something at GitLab may require input from technical contacts from our OSS Partner orgs. Usually someone from the SysAdmin team is listed as the Technical contact.
 * **Legal:** (optional) The person or team who can weigh in on legal matters like updates to terms of service agreements or partnership activities.
 * **Others:** Anyone else who is a key stakeholder to the OSS Partner org. Please specify their role and when to contact.

**Related Documents:**
 * **[OSS Partners Contact Info](https://docs.google.com/spreadsheets/d/1UFaRATA8I2mmcZ-77KBXMoNZCmut5TalZytIzWCh1HQ/edit#gid=484404042)** -- This is confidential, internal information and requires a GitLab team member login. OSS Partners can request updates or removal of information at any time.
 * **[Email Templates](https://docs.google.com/document/d/1et0t3CLQdT8I2UbxBVrpFqMXmnd6cEtMZuE61V1DiXM/edit#heading=h.jc6mgcides78)** -- This document contains email templates that are useful for asking for contact information at the suggested times.

### Public Issue Tracker
We use [public issue trackers](https://gitlab.com/gitlab-org/gitlab/-/boards/1625116?&label_name[]=Open%20Source%20Partners) to help our [OSS Partners](/solutions/open-source/partners/) through and beyond migration.

Public issue trackers help us understand the priority of various feature requests and bugs for our Open Source Partners (priority is denoted as: blockers, urgent, important, and nice-to-have). When OSS Partners set up a public issue tracker, it has the added benefit of helping them get used to our workflow so that they can start giving us more product feedback and helping us improve the product together.

We ask that a representative of the OSS Partner org opens the issue tracker so that they are able to edit the description section. If someone at GitLab opens the issue on the OSS Partner's behalf, the OSS Partner will not have edit access.

Follow these steps to create a public issue tracker:

 1. Create a [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new) in the `gitlab.com/gitlab-org/gitlab` [project](https://gitlab.com/gitlab-org/gitlab/)
 1. Select the `OSS_Partner` template where it prompts you to select a template, and click on `Apply template`
 1. Add in as much information as possible. Once finished, submit it.

 Examples of public issue trackers:
  * [KDE](https://gitlab.com/gitlab-org/gitlab/-/issues/24900)
  * [Eclipse Foundation](https://gitlab.com/gitlab-org/gitlab/-/issues/195865)
  * [Freedesktop](https://gitlab.com/gitlab-org/gitlab/-/issues/217107)

## Project Management for OSS Partners

We use the [GitLab Open Source Partners project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/oss-partners-program/-/boards) to keep internal notes around collaboration with our OSS Partners. This project is accissible only by GitLab team members only as it contains sensitive information that should not be made publicly available. This board also contains a view of all Open Source Partners and has labels to help us identify if they align with any of our GTM messaging.

We use the GitLab Open Source Partners project to track the main issues for the partnership. Issues relating to strategic initiatives with our OSS Parrtner are added to the [`general` OSS program board](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/general/-/boards/1755105) and are linked back to the relevant main OSS Partner issue.

More information about the project management for this program can be found on the main [Community Relations project management page](https://about.gitlab.com/handbook/marketing/community-relations/project-management/).

### Linux Foundation Partnership
We are exploring ways to partner with the Linux Foundation to make it easier for Linux Foundation member projects to apply for and qualify for the GitLab for Open Source program. We are using the [GitLab <> LF project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/linux-foundation) to collaborate with their team on various initiatives.

The application process for LF members projects [can be found here](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94599).FIXME

Note: In addition to having the Linux Foundation as an Open Source Partner, we are a paying member of the Linux Foundation through their consortium membership program. More information about this consortium membership can be found [in the section below](/handbook/marketing/community-relations/opensource-program/#current-memberships).

## Editorial plan for OSS Partners
The Open Source Program Manager worked with the Content team on an editorial strategy to meet the needs of the Open Source Partners program. Posts about technical content and open source partner migrations tend to be very popular, so the Content team collaborates by brainstorming topics and doing reviews.

The plan is to publish posts on the [GitLab Blog](/blog/) that are related to the Open Source Partner program and to feature these on the [Open Source Partners page](/solutions/open-source/partners/).

Marketing opportunities:
 * Migration announcement
 * Migration phase complete
 * Case Study
 * Technical challenge and resolution, use case that aligns with other marketing campaigns such as CI/CD, security, or project management

Resources:
 * [Template - OSS Partner Use Case Blog Posts](https://docs.google.com/document/d/1oRwrwoo6PkuqafAsde11mqCnMvAI2KNZ9E7AfnGg9Fw/edit#) - includes template blog post questions to ask OSS Partners in order to publish posts about migrations, CI/CD, Security, and Project Management use cases.
 * [OSS Blog Posts and Views](https://docs.google.com/spreadsheets/d/1LpgSudtcgXDkPb7XX-4s2PW6vYAQZpDgQRdGw7OWqBY/edit#gid=0)

## How to add a new logo to the Open Source Partners page
Our Open Source Partners are shown on the [Open Source Partners](/solutions/open-source/partners) page and the main [GitLab for Open Source program](/solutions/open-source/) page. These are the steps to add a new logo to those sections.

### 1. Add a new org to the Organizations list
You'll need to add an entry to the [`data/organizations.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/organizations.yml) file using GitLab's WebIDE.

Follow this format (you can copy and paste it at the bottom of the organizations.yml file):

```markdown
- name: ORG NAME
  logo: /images/organizations/logo_ORG NAME.svg
  logo_color: /images/organizations/logo_ORG NAME_color.svg  
  industry_type: 'open source software'
  home_url:  
  landing: false
  opensource_partner: true
```

Example:

```markdown
- name: Inkscape
  logo: /images/organizations/logo_inkscape.svg
  logo_color: /images/organizations/logo_inkscape_color.svg  
  industry_type: 'open source software'
  home_url: https://inkscape.org
  landing: false
  opensource_partner: true
```

### 2. Prep the logos
#### Create two SVGs: one color, one grey
 1. **Get an SVG.** Make sure you have an SVG file. If you weren't provided with one, look for a vector (`*.svg`) version of the logo in the official channels (e.g. the [ARM trademarks page](https://www.arm.com/company/policies/trademarks/guidelines-corporate-logo) has SVG as one of the download options). If you can't find the SVG file on their channels, you may need to Google it and find one on Wikimedia Commons. Download the original file.
 1. **Duplicate SVG and name each copy.** Duplicate the file and name each of the two files as follows:
     * `logo_ORG Name.svg` -- Example: `logo_inkscape.svg`. This will be the greyscale image that will be shown on the customer reference page.
     * `logo_ORG Name_color.svg` -- Example: `logo_inkscape_color.svg`. This will be the color image that displays on the Open Source Partners page.
 1. **Create greyscale logo.** These instructions are specifically for using [Inkscape](https://inkscape.org/) as the image editor, which you can download for free for most operating systems, including Mac.
     * Open the SVG image with Inkscape.
     * In the top menu, go to `Filters` -> `Color` -> `Greyscale`. Click on `Live Preview` so you can see what will happen. Save your changes.
     * Export the logo and make sure that it is called: `logo_ORG Name.svg` as detailed above.

Note: If you are not using Inkscape, aim for getting a greyscale image with Hex Color Code: Dark Grey `#444444` and then export the greyscale logo as an SVG with a transparent background.

#### Optimize the SVGs
Do this for both color and grey logos:
 1. Go to this SVG optimization website: https://jakearchibald.github.io/svgomg/ and upload one of the logos
 1. Make sure that the `Prefer viewBox to width/height` factor is toggled `On`. It should be near the bottom of the list of optimization factors.
 1. Export the SVG and save

### 3. Upload the logo via WebIDE
 * Go to `source/images/organizations` and upload the two prepped logos

Once you've completed these steps, check to make sure that the pipleline works, and check out the preview. If it looks ok, have someone review and merge!

### Review the changes
Unfortunately, the review app will not automatically show you the preview. Here's how to generate a link to preview the changes.

#### Why does the View App not point to the correct webpage?

The View App lets you preview your changes. It detects which files have been changed, and then creates a URL, or a set of URLs, pointing to the built pages with the changes.

Since this type of merge request (adding a new logo to the OSS partners page), does not change any HTML files, the View App does not know where to point to. It defaults to the GitLab home page.

#### How do I get the review app to show the correct page?

Since the `View App` points to the GitLab home page for this build as mentioned above, we need to add the path to the webpage you'd like to preview.

To do this, you need to be familiar with four parts of the View App URL:
1. **The beginning of the URL:** `https://` -- it doesn't include "www"
1. **Your branch name:** You can copy this from your Merge Request (MR) in the "Request to Merge" section directly under the MR description.
1. **The View App snippet:** `.about.gitlab-review.app` -- this is what builds the preview
1. **The path to the webpage you'd like to preview:** This is everything after "about.gitlab.com/" on the webpage you're editing

Example:
1. `https://`
1. `c_hupy-master-patch-24835` (branch name)
1. `.about.gitlab-review.app`
1. `/solutions/open-source/partners` (path to the webpage I want to preview)

This formula creates: `https://c_hupy-master-patch-24835.about.gitlab-review.app/solutions/open-source/partners`. This link allows you to preview the MR changes for the `c_hupy-master-patch-24835` branch on the GitLab Open Source Partners page.

For ease of use, here's the formula:
```
`https://` [your branch] `.about.gitlab-review.app` [rest of the path to the page you want to build]
```

You should now be able to create preview links when the View App doesn't work!

# <i class="far fa-newspaper" id="biz-tech-icons"></i> Consortium Memberships and Sponsorships

GitLab's open source program also oversees GitLab's representation and participation in select industry consortia, as well as GitLab's sponsorship of select open source community events.

## FAQs

### What is a consortium?
We define "consortium" as a group createdto further some technological cause. In the context of open source software, a prototypical consortium would be the [Linux Foundation (LF)](https://en.wikipedia.org/wiki/Linux_Foundation), a non-profit organization founded in 2000 as a merger between Open Source Development Labs and the Free Standards Group, which [hosts and promotes](https://www.linuxfoundation.org/about/) collaborative development of open source software projects.

### Why is consortium marketing important?
Consortia are influential leaders in their respective ecosystems, as they often host conferences and underwrite programs that influence global conversations about particular technological developments. Participating in consortia enhances GitLab's brand—and helps align GitLab's engineering efforts with global efforts and trends.

### How does GitLab particpate in consortium activities?
While select consortium memberships fall within the purview (and budget) of GitLab's open source program, the [Developer Evangelism team](/handbook/marketing/community-relations/developer-evangelism/) focuses on consortium marketing, working to integrate GitLab's overall community message and technical perspective into the most appropriate and effective industry conversations.

### How can I recommend GitLab get involved in a consortium?
You can open an issue in the [Consortium Memberships project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships). When you do, please use the `membership-evaluation` template to structure your issue. Open source program team members will evaluate your application using the following criteria. When we review the application, we'll assess it with these considerations in mind:

* Awareness opportunities
* Ease of collaboration
* Contribution and hiring pool

| Considerations | What we're interested in | Questions we're asking |
| --------------| ------------------ | -----------------------------|
| Awareness opportunities | Size of the organization<br /><br />Frequency and impact of marketing opportunities<br /><br />| How many authenticated and non-authenticated users are visiting organization's website monthly?<br /><br />How many people are part of the organization’s community?<br /><br />What sorts of marketing and communication channels (social media platforms, newsletters, blogs, events) does the organization use?<br /><br /> Will GitLab appear in those official channels? How prominent would our placement be? | 
| Ease of collaboration | Access to a dedicated marketing resources/point person<br /><br />Time-to-execute for standard communication types | Does the organization have marketing capacity?<br /><br />How mature is the organization's brand and marketing portions?<br /><br />How quickly can this organization produce a resource (e.g., a case study)? A week? A month? A quarter?<br /><br />How responsive is the person in charge of the relationship?<br /><br />Is marketing handled by volunteers or paid employees? |
| Contribution and hiring pool | Size of contributor/member base<br /><br />Overall community/member activity<br /><br />Frequency of community contribution<br /><br />Rate of adoption | How active is the community the organization is attempting to foster?<br /><br /> Does the organization have a sense of its community's health?<br /><br />Do we see hiring opportunities opportunities to recruit from the community's talent pool?<br /><br />What is the growth of the community or foundation itself?<br /><br />Do we see job opportunities within that software ecosystem (are people hiring contributors from this community in general)?<br /><br />How can GitLab contribute in ways that align with our interests?<br /><br />Can GitLab participate in the project's roadmap in ways that creates mutual value? |

### In which consortia is GitLab involved?
We are currently members of the following consortia:

* [Continuous Delivery Foundation](https://cd.foundation/)
* [Cloud Native Computing Foundation](https://www.cncf.io/)
* [Fintech Open Source Foundation](https://www.finos.org/)
* [InnerSource Commons](https://innersourcecommons.org/)
* [Linux Foundation](https://www.linuxfoundation.org/)
* [Open Source Security Foundation](https://openssf.org/)
* [TODO Group](https://todogroup.org/)

[Complete details of GitLab's activities](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships) with these groups are available in the `Consortium Memberships` project. Note that because this project contains sensitive data and personally identifying information, it is only accessible to GitLab team members.

## Elections for Board of Directors opportunities
Some of the consortia in which we participate allow members to run for their respective Boards of Directors. Anyone interested in becoming more involved in any of the consortia GitLab supports should visit the `Consortium Memberships` [project](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/consortium-memberships) and open an issue.

Review the information below if you're thinking of seeking nomination for (or election to) consortium positions.

### Internal nominations
Create an issue for each nomination so GitLab team members who are also interested in running can discuss, and so additional internal nominations can occur. Community Relations, Alliances, and Marketing leadership (CMO, Director of Corporate Marketing) teams will likely be involved. Prepare a [nomination statement](https://docs.google.com/document/d/1IrtEBGfjuwi8Tz87U0vOsCxN9-f2-Krt1t-db87nyzU/edit?ts=60c13066#heading=h.5tbh3ex4b8al) that explains your interest in joining the board. Post this as part of your issue so our internal teams can help you polish it.

### Campaigning
Once GitLab candidates are nominated, the Community Relations team can help them campaign for their positions. We'll make other GitLab team members  aware of the election and equip them to assist your campaign, too (e.g., by announcing the campaign on the `#whats-happening-at-gitlab` Slack channel).

### Promoting
The social media team is able to promote elections notification news. They simply need a place to point people, preferably an updated webpage that lists the board of directors or a social media post from the organization that mentions the election results.

## Event sponsorships
GitLab's open source program has a small budget to sponsor events that allow GitLab to engage with and build relationships among open source partner communities. GitLab's [field marketing team](/handbook/marketing/field-marketing/#3rd-party-events) manages all other event sponsorship requests.  

The open source program team tracks event partication in the `Open Source Marketing` [project on GitLab](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/open-source-marketing). To suggest an open source event sponsorship, open an issue in this project and use the `event` issue template to file your request.

## Sponsorship resources
Event organizers and consortium leads working with GitLab will find GitLab's brand-related assets (such as logo files, press release boilerplate, and trademark information) in [GitLab's press kit](https://about.gitlab.com/press/press-kit/).

# <i class="far fa-newspaper" id="biz-tech-icons"></i> Measuring our success

Our team measures the success of our work in the following ways.

## Sisene
We use the [GitLab for Open Source KPI Dashboard](https://app.periscopedata.com/app/gitlab/670411/Open-Source-Program) on Sisene to track program metrics. This dashboard's visibility is internal to GitLab team members only and measures the following:

**Current status stats:**
These stats give us an idea of how many active users we have as part of the GitLab for Open Source program.
 * **Active projects** - number of program members that have an active new application or renewal.
 * **Active seats** - number of users under the GitLab for Open Source program. Since we do not enable Telemetry, this number is taken from the number of seats program members request.

**Stats counted from program inception:**
Lifetime stats of how the program has performed since its creation.
 * **Number of licenses** - these are licenses issued since the beginning of the program (includes add-ons, and renewals).
 * **Enrolled projects** - how many unique projects have enrolled since the program began.
 * **Projects renewed** - the percentage of projects that have renewed since program began.
 * **Seats issued** - total seats issued since beginning of program.

**Quarterly stats:**
 * New projects enrolled per quarter
 * Number of seats granted per quarter

## Impressions Dashboard
We keep track of OSS impressions in the [OSS Impressions Dashboard](https://datastudio.google.com/u/0/reporting/b62b70f4-ed1d-4f0a-8ed4-ebbcbca29962/page/YsgmB).

This dashboard takes into account:

 * Blogs
 * YouTube
 * Social media impressions
 * Media (PR, placed articles)
 * Events

The dashboard is generated by the content in:
 * [OSS Marketing Spreadsheet](https://docs.google.com/spreadsheets/d/1LpgSudtcgXDkPb7XX-4s2PW6vYAQZpDgQRdGw7OWqBY/edit#gid=0) -- Blogs, YouTube, Social Media, Media
 * [OSS Marketing Board](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/oss-marketing/-/boards) -- Events

## Cumulative Data
We only use cumulative metrics in external presentations as per our [communication guidelines (#8 of presentation section)](https://about.gitlab.com/handbook/communication/#presentations).

OSS cumulative metrics are kept in a spreadsheet: [OSS Cumulative Metrics](https://docs.google.com/spreadsheets/d/1wr1G7N9-XiQmfkIcq3wNvS3JE0-EdOOAkqpG5TvkW6o/edit#gid=1969501712). They include the number of new projects and new seats per fiscal quarter.

To generate these cumulative graphs:
1. Go to the [OSS dashboard on Sisene](https://app.periscopedata.com/app/gitlab/670411/Open-Source-Program).  
1. Download `New projects enrolled per fiscal quarter` and `Number of new seats issued per fiscal quarter` by clicking on the hamburger menu by each and clicking on **Download Data**.
1. Go to the [OSS Cumulative Metrics](https://docs.google.com/spreadsheets/d/1wr1G7N9-XiQmfkIcq3wNvS3JE0-EdOOAkqpG5TvkW6o/edit#gid=1969501712) sheet and copy any existing tab for 'New Projects` and `New Seats`. Paste the data from the relevant Sisene downloads there. This will generate new graphs that you can use to display cumulative metrics per fiscal quarter.

## Hacker News Topics
We also track (and, when necessary, participate in) Hacker News discussions related to both our open source programs and partners. Examples include:

- 2022-06-14: [Gitlab Now the Main Development Platform for Wine](https://news.ycombinator.com/item?id=31737807)
- 2020-10-28: [Wikimedia is moving to Gitlab](https://news.ycombinator.com/item?id=24919569)
- 2020-06-29: [The KDE community is moving to GitLab](https://news.ycombinator.com/item?id=23679360)
- 2018-05-31: [Gnome has moved to GitLab](https://news.ycombinator.com/item?id=17198610)
- 2019-09-30: [KDE is adopting GitLab](https://news.ycombinator.com/item?id=21112632)
- 2017-11-15: [Debian and GNOME announce plans to migrate communities to GitLab](https://news.ycombinator.com/item?id=15701922)
- 2017-05-16: [A proposal to move Gnome to GitLab](https://news.ycombinator.com/item?id=14352521)
